console.log( )
console.log('===== Soal 1 Range =====')
console.log( )

function range(startNum, finishNum) {
    var rangeArr = [];
    if (startNum > finishNum) {
        var rangelength = startNum - finishNum + 1;
        for (var a = 0; a< rangelength; a++){
            rangeArr.push(startNum - a)
        }
    }else if (startNum , finishNum){
        var rangelength = finishNum - startNum + 1;
        for (var a = 0; a< rangelength; a++){
            rangeArr.push(startNum + a)
        }
    }else if (!startNum || !finishNum){
        return -1
    }
    return rangeArr
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log( )
console.log('===== Soal 2 Range with Step =====')
console.log( )

function rangewithstep (startNum, finishNum, step){
    var rangeArr = [];

    if (startNum > finishNum){
        var currentnum = startNum;
        for (var a = 0; currentnum >= finishNum; a++){
            rangeArr.push(currentnum)
            currentnum -= step
        }
    }else if (startNum < finishNum){
        var currentnum =startNum;
        for (var a = 0; currentnum <= finishNum; a++){
            rangeArr.push(currentnum)
            currentnum += step
        }
    }else if (!startnum || !finishNum || step){
        return -1
    }
    
    return rangeArr
}
console.log(rangewithstep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangewithstep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangewithstep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangewithstep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log( )
console.log('===== Soal 3 Sum of Range =====')
console.log( )

function sum(startNum, finishNum, step){
    var rangeArr = [ ];
    var distance;

    if(!step){
        distance = 1
    }else {
        distance = step
    }

    if (startNum > finishNum){
        var currentnum = startNum;
        for (var a = 0; currentnum >= finishNum; a++){
            rangeArr.push(currentnum)
            currentnum -= distance
        }
    }else if (startNum < finishNum){
        var currentnum = startNum;
        for (var a = 0; currentnum <= finishNum; a++){
            rangeArr.push(currentnum)
            currentnum += distance
        }
    }else if (!startNum && !finishNum && !step){
        return 0
        
    }else if (startNum){
        return startNum
    }
    var total = 0

    for (var a = 0; a<rangeArr.length; a++){
        total = total + rangeArr[a]    
    }
    return  total
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log( )
console.log('===== Soal 4 Array Multidimensi =====')
console.log( )

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function datahandling (data) {
    var datalength = data.length
    for (var a = 0; a<datalength; a++){
        var id = 'Nomor ID : '+ data[a][0]
        var nama = 'Nama Lengkap : '+ data[a][1]
        var ttl = 'Tempat/Tgl.Lahir : '+ data [a][2] + ' ' + data [a][3]
        var hobi = 'Hobi : '+ data[a][4]  
        console.log( )

        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)
    }
}

datahandling(input)

console.log( )
console.log('===== Soal 5 Balik Kata =====')
console.log( )

function balikkata(kata){
    var kata2 =' '
    for (var a = kata.length-1; a >=0; a--){
        kata2 += kata[a]
    }
    return kata2
}


console.log(balikkata("Kasur Rusak")) // kasuR rusaK
console.log(balikkata("SanberCode")) // edoCrebnaS
console.log(balikkata("Haji Ijah")) // hajI ijaH
console.log(balikkata("racecar")) // racecar
console.log(balikkata("I am Sanbers")) // srebnaS ma I 

console.log( )
console.log('===== Soal 6 Metode Array =====')
console.log( )

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

function dataHandling2(data){
    var databaru = data
    var namabaru = data[1] + ' Elsharawy'
    var provbaru = 'Provinsi ' + data[2]
    var gender = 'Pria'
    var sekolah = 'SMA International Metro'

    databaru.splice(1, 1, namabaru)
    databaru.splice(2, 1, provbaru)
    databaru.splice(4, 1, gender, sekolah)

    var arrtgl = data [3]
    var tglbaru = arrtgl.split('/')
    var bulan = tglbaru[1]
    var namabulan = ' '

    switch(bulan){
        case '01' :
            namabulan = 'Januari'
            break;
        case '02' :
            namabulan = 'Februari'
            break;
        case '03' :
            namabulan = 'Maret'
            break;
        case '04' :
            namabulan = 'April'
            break;
        case '05' :
            namabulan = 'Mei'
            break;
        case '06' :
            namabulan = 'Juni'
            break;
        case '07' :
            namabulan = 'Juli'
            break;
        case '08' :
            namabulan = 'Agustus'
            break;
        case '09' :
            namabulan = 'September'
            break;
        case '10' :
            namabulan = 'Oktober'
            break;
        case '11' :
            namabulan = 'November'
            break;
        case '12' :
            namabulan = 'Desember'
            break;
        default :
            break;
    }
    var tgljoin =  tglbaru.join(' ')
    var arrtgl = tglbaru.sort(function(value1, value2){
        value2 - value1
    })
    var editnama = namabaru.slice(0, 15)
    console.log(databaru)
    console.log(namabulan)
    console.log(arrtgl)
    console.log(tgljoin)
    console.log(editnama)
}

console.log(" ")

 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 

