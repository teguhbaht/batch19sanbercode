console.log( )
console.log('===== Soal 1 - Looping While =====')
console.log('======== Looping Pertama ========')
console.log( )

var angka = 2;
while(angka <= 20){
    console.log(angka + ' ' + '- I Love Coding');
    angka+=2;
}

console.log( )

console.log('======== Looping Kedua ========')
console.log( )

var angka2 = 20;
while(angka2 >= 2){
    console.log(angka2 + ' ' + '- I will become a mobile developer');
    angka2-=2;
}

console.log(' ')

console.log('===== Soal 2 - Looping For =====')
console.log(' ')

var santai = '- Santai'
var berkualitas = '- Berkualitas'
var lovecoding = '- I love coding'

for(var num = 1; num <=20; num++){
    if(num % 2 !=1){
        console.log(num + ' ' + berkualitas)
    }else if(num % 3 == 0){
        console.log(num + ' ' + lovecoding)
    }else{
        console.log(num + ' ' + santai)
    }
}

console.log(' ')

console.log('===== Soal 3 - Membuat Persegi Panjang =====')
console.log(' ')

var a = 1;
var b = 1;
var panjang = 8;
var lebar = 4;
var pagar = ''

while(b <= lebar){
    while(a <= panjang){
        pagar +='#';
        a++
    }
    console.log(pagar);
    pagar = '';
    a = 1;
    b++;
}

console.log(' ')

console.log('===== Soal 4 - Membuat Tangga =====')
console.log(' ')

var c = 1;
var d = 1;
var alas = 7;
var tinggi = 7;
var pagar = ''

for(var c = 1; c <= tinggi; c++){
    for(var d = 1; d <= c; d++){
        pagar +='#';
    }
    console.log(pagar)
    pagar = '';

}
console.log(' ')

console.log('===== Soal 5 - Membuat Papan Catur =====')
console.log(' ')

var e = 1; 
var f = 1;
var panjang = 8;
var lebar = 8;
var papan = ' '

for(var f = 1; f <= lebar; f++){
    if(f % 2 == 1){
        for(e = 1; e <= panjang; e++){
            if(e % 2 == 1){
                papan +=' '
            }else {
                papan +='#'
            }
        }
    }else {
        for(e = 1; e <= panjang; e++){
            if(e % 2 == 1){
                papan +='#'
            }else {
                papan +=' '
            }
        }
    }console.log(papan);
    papan = ' ';
}

console.log(' ')