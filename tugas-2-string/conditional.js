/*
Soal 5 Conditional
Untuk memulai game pemain harus memasukkan variable nama dan peran. 
Jika pemain tidak memasukkan nama maka game akan mengeluarkan warning “Nama harus diisi!“. 
Jika pemain memasukkan nama tapi tidak memasukkan peran maka game akan mengeluarkan warning 
“Pilih Peranmu untuk memulai game“. Terdapat tiga peran yaitu penyihir, guard, dan werewolf. 
*/   
console.log('========== Soal 1 Conditional ==========')
console.log(' ')

var nama = ' '
var peran = ' '

if (nama == ' '){
    console.log('Nama harus diisi!')
}
else if (nama && peran == ' '){
    console.log('Halo' + " " + nama + ',Pilih peranmu untuk memulai game!')
}
else if (nama == 'Jane' && peran == 'Penyihir'){
    console.log('Selamat datang di Dunia Werewolf, Jane \nHalo Penyihir Jane, kamu dapat melihat siapa saja yang menjadi werewolf')
}
else if (nama == 'Jenita' && peran == 'Guard'){
    console.log('Selamat datang di dunia werewolf, Jenita \nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf')
}
else if (nama == 'Junaedi' && peran == 'Werewolf'){
    console.log('Selamat datang di dunia werewolf, Junaedi \nHalo Werewolf Junaedi, kamu akan memakan mangsa setiap malam!')
}

console.log(' ')
console.log('========== Soal 2 Conditional ==========')
console.log(' ')

var hari = 7
var bulan = 11
var tahun = 1990

var namaBulan;

if (hari >=1 && hari <=31){
    if (bulan >=1 && bulan <=12){
        if (tahun >=1900 && tahun <=2200){
            switch (bulan){
                case 1 :
                    namaBulan = 'Januari';
                    break;
                case 2 :
                    namaBulan = 'Februari';
                    break;
                case 3 :
                    namaBulan = 'Maret';
                    break;
                case 4 :
                    namaBulan = 'April';
                    break;
                case 5 :
                    namaBulan = 'Mei';
                    break;
                case 6 :
                    namaBulan = 'Juni';
                    break;
                case 7 :
                    namaBulan = 'Juli';
                    break;
                case 8 :
                    namaBulan = 'Agustus';
                    break;
                case 9 :
                    namaBulan = 'September';
                    break;
                case 10 :
                    namaBulan = 'Oktober';
                    break;
                case 11 :
                    namaBulan = 'November';
                    break;
                case 12 :
                    namaBulan = 'Desember';
                    break;
                default :
                    break;
            }
            console.log(hari + ' ' + namaBulan + ' ' + tahun)
        }else{
            console.log('masukan tahun diantara (1900-2200)')
            }
    }else{
        console.log('masukan bulan diantara (1-12)')
        }
    }else{
        console.log('masukab tanggal diantara (1-31')
}

console.log(' ')





/*var buttonPushed = 0;
   switch(buttonPushed) {
     case 1:   { console.log('matikan TV!'); break; }
     case 2:   { console.log('turunkan volume TV!'); break; }
     case 3:   { console.log('tingkatkan volume TV!'); break; }
     case 4:   { console.log('matikan suara TV!'); break; }
     default:  { console.log('Tidak terjadi apa-apa'); }}*/

     /*var hari = 1
     var bulan = 5
     var tahun = 1945
     
     switch(bulan) {
         case 1: {console.log('Januari'); break;}
         case 2: {console.log('Februari'); break;}
         case 3: {console.log('Maret'); break;}
         case 4: {console.log('April'); break;}
         case 5: {console.log('Mei'); break;}
         case 6: {console.log('Juni'); break;}
         case 7: {console.log('Juli'); break;}
         case 8: {console.log('Agustus'); break;}
         case 9: {console.log('September'); break;}
         case 10: {console.log('Oktober'); break;}
         case 11: {console.log('November'); break;}
         case 12: {console.log('Desember'); break;}
     } 
     console.log(tampilan)*/
     